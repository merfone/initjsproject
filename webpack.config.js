const path = require('path')
const webpack = require('webpack')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const ManifestPlugin = require('webpack-manifest-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const dev = process.env.NODE_ENV === "dev"

let cssLoaders = [
    { loader: 'css-loader', options: {importLoaders: 1, minimize: !dev} }
]

if (!dev) {
    cssLoaders.push({ 
        loader: 'postcss-loader',
        options: {
            plugins: (loader) => [
                require('autoprefixer') ({
                    browsers: ['last 2 versions', 'ie > 8']
                })
            ]
        }
    })
}

let config = {
    entry: {
        app: [
            './assets/css/app.scss',
            './assets/js/app.js'
        ] 
    },
    watch: dev,
    output: {
        path: path.resolve('./public/assets'),
        filename: dev ? '[name].js' : '[name].[chunkhash:8].js',
        publicPath: dev ? '/' : './'
    },
    resolve: {
        alias: {
            '@css': path.resolve('./assets/css/'),
            '@': path.resolve('./assets/js/'),
            '@img': path.resolve('./assets/img/')
        }
    },
    devtool: dev ? "cheap-module-eval-source-map" : false,
    devServer: {
        overlay: true,
        contentBase: path.resolve('./assets')
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: ['eslint-loader']
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader' 
                }
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: cssLoaders
                })
            },
            {
                test: /\.scss$/,
                use: dev ? ['style-loader', ...cssLoaders, 'sass-loader'] : ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [...cssLoaders, 'sass-loader']
                }) 
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: './fonts/'
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                        limit: 8192,
                        name: '[name].[hash:7].[ext]',
                        outputPath: './img/'
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    {
                        loader: 'img-loader',
                        options: {
                            enabled: !dev
                        }
                    }
                  
                ]
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: dev ? '[name].css' : '[name].[contenthash:8].css',
            //disable: dev
        }),
        new HtmlWebpackPlugin(),
        new webpack.EnvironmentPlugin({
            NODE_ENV: dev ? 'dev' : 'prod',
            DEBUG: false
        })
    ]
}

if(!dev) {
    config.plugins.push(new UglifyJSPlugin({
        sourceMap: false
    }))
    config.plugins.push(new ManifestPlugin())
    config.plugins.push(new CleanWebpackPlugin(['public/assets'], {
        root: path.resolve('./'),
        verbose: true,
        dry: false
    }))
}

module.exports = config