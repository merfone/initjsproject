'use strict';

const fs = require('fs');
const { sep } = require('path');
const { resolve } = require('path');
const DEFAULT_CONFIG_FILE = resolve(__dirname, '../initproject.default.json')

function sepReplacement(path) {
  return path.replace('/', sep);
}

let NewProject = function(config) {
  this.configFile = (config) ? config : DEFAULT_CONFIG_FILE;
}

NewProject.prototype.init = function() {
  fs.access(this.configFile, fs.constants.R_OK, (err) => {
    this.loadConfig(this.configFile, 'utf8')
    this.createDirectories();
    this.createFiles();
  })
};
NewProject.prototype.initWebpack = function() {
  fs.copyFile(resolve(__dirname, '../webpack.config.js'), resolve('./webpack.config.js'), (err) => {
    if (!err) {
      //console.log(resolve(__dirname, '../webpack.config.js') + ' was copied to ' + resolve('./webpack.config.js'));      
    } else {
      console.log('ERROR! Copying is impossible!')
    }
  });
}
NewProject.prototype.loadConfig = function(configFile, encoding) {
  let config = fs.readFileSync(configFile, {encoding: encoding});
  this.directories = JSON.parse(config).directories;
  this.files = JSON.parse(config).files;
}

NewProject.prototype.createDirectories = function() {
  if (this.directories) {
    console.log('\nDirectories creation :');
    for (let j = 0, lengthJ = this.directories.length; j < lengthJ; ++j) {
      let dir = sepReplacement(this.directories[j]);

      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
        console.log('Directory "' + dir + '" created!');
      } else {
        console.error( 'ERROR! "' + dir + '" already exists' );
      }

    }
  } else {
    console.log('ERROR! "directories" undefined in config file' + this.configFile)
  }
}

NewProject.prototype.createFiles = function() {
  if (this.files) {
    console.log('\nFiles creation :');
    for (let i = 0, lengthJ = this.files.length; i < lengthJ; ++i) {
      let filePath = sepReplacement(this.files[i].file);
      let fileContent = this.files[i].content;
      
      if (!fs.existsSync(filePath)) {
          fs.writeFileSync(filePath, (fileContent) ? fileContent : '');
          console.log('File "' + filePath + '" created!');
        } else {
          console.error( 'ERROR! "' + filePath + '" already exists' );
        }

    }
  } else {
    console.log('ERROR! "files" undefined in config file' + this.configFile)
  }
}

module.exports = NewProject;

