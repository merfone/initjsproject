#!/usr/bin/env node

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Arthur Varennes
*/

'use strict'

const { resolve } = require('path');
const NewProject = require('../lib/NewProject')

/* Init New Project */
let myProject = new NewProject();
myProject.init();
myProject.initWebpack();
